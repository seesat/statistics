# Statistics

Scripts for statistical analysis

## Getting started

All scripts within this project are written in RStudio. An appropriate project file resides in every sub-folder.

You can obtain RStudio [here](https://www.rstudio.com) and the R programming language [here](https://www.r-project.org).

All scripts are distributed under the [MIT licence](https://gitlab.com/seesat/statistics/-/blob/develop/LICENSE).
