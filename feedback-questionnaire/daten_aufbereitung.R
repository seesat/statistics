# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Datenaufbereitung fuer "Feedbackbogen SeeSat-Studienarbeiten"
# 
# 2021-07-24   Stefan Wertheimer   Erste Version
# 2021-08-13   Stefan Wertheimer   Anpassungen beim ersten Einsatz
# 2022-09-18   Stefan Wertheimer   Pattern Matching für Umbenennung der Zeilen
#                                  Anpassung auf Datensatz 2021/2022:
#                                     - Änderung input_filename'
#                                     - Auskommentieren 'p6_5_workshop_themen'
#                                     - Reduktion auf 10-seitigen Fragebogen
# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

library(tidyverse)

input_filename <- '2021-2022_all_data_2022-08-13_bereinigt.txt'
filename <- str_sub(input_filename, 1, -5)

#### Quelldatei einlesen ####
results <- read.delim(file=input_filename, header=T, sep="\t", dec=",")

#### Leere Spalte am Ende des Exports loeschen ####
results[names(results) == 'X'] <- list(NULL)

#### Umbenennung der Spalten ####
names(results)[names(results) == 'id_user'] <- 'id'
# Spaltenname "user" ist ok
# Spaltenname "session" ist ok

### Seite 2: "Fragen zur Studienarbeit (1/3)"
names(results)[grep('p_2_2',  names(results))] <- 'p2_1_verstaendlichkeit'
names(results)[grep('p_3_2',  names(results))] <- 'p2_2_umfang'
names(results)[grep('p_4_2',  names(results))] <- 'p2_3_faehigkeit'
names(results)[grep('p_5_2',  names(results))] <- 'p2_4_unterstuetzung'
names(results)[grep('p_6_2',  names(results))] <- 'p2_5_antwortzeiten'
### Seite 3: "Fragen zur Studienarbeit (2/3)"
names(results)[grep('p_2_3',  names(results))] <- 'p3_1_zusammenhang'
names(results)[grep('p_3_3',  names(results))] <- 'p3_2_ansprechpartner'
names(results)[grep('p_4_3',  names(results))] <- 'p3_3_problemloesung'
names(results)[grep('p_5_3',  names(results))] <- 'p3_4_betreuung_kommunikation'
names(results)[grep('p_6_3',  names(results))] <- 'p3_5_betreuung_treffen'
### Seite 4: "Fragen zur Studienarbeit (3/3)"
names(results)[grep('p_2_4',  names(results))] <- 'p4_1_aufgabenplanung'
names(results)[grep('p_3_4',  names(results))] <- 'p4_2_wissenschaftliches_arbeiten'
names(results)[grep('p_4_4',  names(results))] <- 'p4_3_fachliche_kenntnisse'
names(results)[grep('p_5_4',  names(results))] <- 'p4_4_neue_kenntnisse'

### Seite 5: "Fragen zur Projektorganisation (1/2)"
names(results)[grep('p_2_5',  names(results))] <- 'p5_1_gruppen_kommunikation'
names(results)[grep('p_3_5',  names(results))] <- 'p5_2_gruppen_treffen'
names(results)[grep('p_4_5',  names(results))] <- 'p5_3_plenum_treffen'
names(results)[grep('p_5_5',  names(results))] <- 'p5_4_treffen_moderation'
names(results)[grep('p_6_5',  names(results))] <- 'p5_5_treffen_ziel'
### Seite 6: "Fragen zur Projektorganisation (2/2)"
names(results)[grep('p_2_6',  names(results))] <- 'p6_1_workshop_organisation'
names(results)[grep('p_3_6',  names(results))] <- 'p6_2_workshop_mehrwert'
names(results)[grep('p_4_6',  names(results))] <- 'p6_3_workshop_beitrag'
names(results)[grep('p_5_6',  names(results))] <- 'p6_4_workshop_anzahl'
#names(results)[grep('p_6_6',  names(results))] <- 'p6_5_workshop_themen'

### Seite 7: "Fragen zur Arbeitsumgebung (1/2)"
names(results)[grep('p_2_7',  names(results))] <- 'p7_1_nextcloud_zugang'
names(results)[grep('p_3_7',  names(results))] <- 'p7_2_nextcloud_benutzung'
names(results)[grep('p_4_7',  names(results))] <- 'p7_3_nextcloud_erreichbarkeit'
names(results)[grep('p_5_7',  names(results))] <- 'p7_4_nextcloud_uebersicht'
names(results)[grep('p_6_7',  names(results))] <- 'p7_5_nextcloud_online_bearbeitung'
### Seite 8: "Fragen zur Arbeitsumgebung (2/2)"
names(results)[grep('p_2_8',  names(results))] <- 'p8_1_nextcloud_ob_ausreichend'
names(results)[grep('p_3_8',  names(results))] <- 'p8_2_nextcloud_ob_einfach'
names(results)[grep('p_5_8',  names(results))] <- 'p8_3_nextcloud_verbesserung'
names(results)[grep('p_7_8',  names(results))] <- 'p8_4_textverarbeitung'
names(results)[grep('p_9_8',  names(results))] <- 'p8_5_andere_tools'
names(results)[grep('p_11_8',  names(results))] <- 'p8_6_fehlende_tools'

### Seite 9: "Allgemeine Fragen (1/2)"
names(results)[grep('p_2_9',  names(results))] <- 'p9_1_schulnote'
names(results)[grep('p_4_9',  names(results))] <- 'p9_2_gute_eindruecke'
names(results)[grep('p_6_9',  names(results))] <- 'p9_3_schlechte_eindruecke'
names(results)[grep('p_8_9',  names(results))] <- 'p9_4_verbesserungen'
### Seite 10: "Allgemeine Fragen (2/2)"
names(results)[grep('p_2_10',  names(results))] <- 'p10_1_weiterempfehlung'
names(results)[grep('p_3_10',  names(results))] <- 'p10_2_info_projektfortschritt'
names(results)[grep('p_4_10',  names(results))] <- 'p10_3_aktiver_beitrag'
names(results)[grep('p_5_10',  names(results))] <- 'p10_4_vereinsbeitritt'

### Zeitstempel
names(results)[names(results) == 'date..1'] <- 'start_seite_1'
names(results)[names(results) == 'date..2'] <- 'start_seite_2'
names(results)[names(results) == 'date..3'] <- 'start_seite_3'
names(results)[names(results) == 'date..4'] <- 'start_seite_4'
names(results)[names(results) == 'date..5'] <- 'start_seite_5'
names(results)[names(results) == 'date..6'] <- 'start_seite_6'
names(results)[names(results) == 'date..7'] <- 'start_seite_7'
names(results)[names(results) == 'date..8'] <- 'start_seite_8'
names(results)[names(results) == 'date..9'] <- 'start_seite_9'
names(results)[names(results) == 'date..10'] <- 'start_seite_10'
#names(results)[names(results) == 'date..11'] <- 'start_seite_11'

#### Umkodierung der Datentypen ########session Umkodierung der Datentypen ####
results$id <- as.numeric(results$id)
results$user <- as.character(results$user)
results$session <- as.character(results$session)
### Seite 2
results$p2_1_verstaendlichkeit <- as.numeric(results$p2_1_verstaendlichkeit)
results$p2_2_umfang <- as.numeric(results$p2_2_umfang)
results$p2_3_faehigkeit <- as.numeric(results$p2_3_faehigkeit)
results$p2_4_unterstuetzung <- as.numeric(results$p2_4_unterstuetzung)
results$p2_5_antwortzeiten <- as.numeric(results$p2_5_antwortzeiten)
### Seite 3
results$p3_1_zusammenhang <- as.numeric(results$p3_1_zusammenhang)
results$p3_2_ansprechpartner <- as.numeric(results$p3_2_ansprechpartner)
results$p3_3_problemloesung <- as.numeric(results$p3_3_problemloesung)
results$p3_4_betreuung_kommunikation <- as.numeric(results$p3_4_betreuung_kommunikation)
results$p3_5_betreuung_treffen <- as.numeric(results$p3_5_betreuung_treffen)
### Seite 4
results$p4_1_aufgabenplanung <- as.numeric(results$p4_1_aufgabenplanung)
results$p4_2_wissenschaftliches_arbeiten <- as.numeric(results$p4_2_wissenschaftliches_arbeiten)
results$p4_3_fachliche_kenntnisse <- as.numeric(results$p4_3_fachliche_kenntnisse)
results$p4_4_neue_kenntnisse <- as.numeric(results$p4_4_neue_kenntnisse)
### Seite 5
results$p5_1_gruppen_kommunikation <- as.numeric(results$p5_1_gruppen_kommunikation)
results$p5_2_gruppen_treffen <- as.numeric(results$p5_2_gruppen_treffen)
results$p5_3_plenum_treffen <- as.numeric(results$p5_3_plenum_treffen)
results$p5_4_treffen_moderation <- as.numeric(results$p5_4_treffen_moderation)
results$p5_5_treffen_ziel <- as.numeric(results$p5_5_treffen_ziel)
### Seite 6
results$p6_1_workshop_organisation <- as.numeric(results$p6_1_workshop_organisation)
results$p6_2_workshop_mehrwert <- as.numeric(results$p6_2_workshop_mehrwert)
results$p6_3_workshop_beitrag <- as.numeric(results$p6_3_workshop_beitrag)
results$p6_4_workshop_anzahl <- as.numeric(results$p6_4_workshop_anzahl)
#results$p6_5_workshop_themen <- as.character(results$p6_5_workshop_themen)
### Seite 7
results$p7_1_nextcloud_zugang <- as.numeric(results$p7_1_nextcloud_zugang)
results$p7_2_nextcloud_benutzung <- as.numeric(results$p7_2_nextcloud_benutzung)
results$p7_3_nextcloud_erreichbarkeit <- as.numeric(results$p7_3_nextcloud_erreichbarkeit)
results$p7_4_nextcloud_uebersicht <- as.numeric(results$p7_4_nextcloud_uebersicht)
results$p7_5_nextcloud_online_bearbeitung <- factor(results$p7_5_nextcloud_online_bearbeitung, levels=1:2, labels=c("ja", "nein"))
### Seite 8
results$p8_1_nextcloud_ob_ausreichend <- as.numeric(results$p8_1_nextcloud_ob_ausreichend)
results$p8_2_nextcloud_ob_einfach <- as.numeric(results$p8_2_nextcloud_ob_einfach)
results$p8_3_nextcloud_verbesserung <- as.character(results$p8_3_nextcloud_verbesserung)
results$p8_4_textverarbeitung <- as.character(results$p8_4_textverarbeitung)
results$p8_5_andere_tools <- as.character(results$p8_5_andere_tools)
results$p8_6_fehlende_tools <- as.character(results$p8_6_fehlende_tools)
### Seite 9
results$p9_1_schulnote <- as.numeric(results$p9_1_schulnote)
results$p9_2_gute_eindruecke <- as.character(results$p9_2_gute_eindruecke)
results$p9_3_schlechte_eindruecke <- as.character(results$p9_3_schlechte_eindruecke)
results$p9_4_verbesserungen <- as.character(results$p9_4_verbesserungen)
### Seite 10
results$p10_1_weiterempfehlung <- factor(results$p10_1_weiterempfehlung, levels=1:2, labels=c("ja", "nein"))
results$p10_2_info_projektfortschritt <- factor(results$p10_2_info_projektfortschritt, levels=1:2, labels=c("ja", "nein"))
results$p10_3_aktiver_beitrag <- factor(results$p10_3_aktiver_beitrag, levels=1:2, labels=c("ja", "nein"))
results$p10_4_vereinsbeitritt <- factor(results$p10_4_vereinsbeitritt, levels=1:2, labels=c("ja", "nein"))
### Zeitstempel
results$start_seite_1 <- strptime(results$start_seite_1, format="%d.%m.%Y - %H:%M:%S")
results$start_seite_2 <- strptime(results$start_seite_2, format="%d.%m.%Y - %H:%M:%S")
results$start_seite_3 <- strptime(results$start_seite_3, format="%d.%m.%Y - %H:%M:%S")
results$start_seite_4 <- strptime(results$start_seite_4, format="%d.%m.%Y - %H:%M:%S")
results$start_seite_5 <- strptime(results$start_seite_5, format="%d.%m.%Y - %H:%M:%S")
results$start_seite_6 <- strptime(results$start_seite_6, format="%d.%m.%Y - %H:%M:%S")
results$start_seite_7 <- strptime(results$start_seite_7, format="%d.%m.%Y - %H:%M:%S")
results$start_seite_8 <- strptime(results$start_seite_8, format="%d.%m.%Y - %H:%M:%S")
results$start_seite_9 <- strptime(results$start_seite_9, format="%d.%m.%Y - %H:%M:%S")
results$start_seite_10 <- strptime(results$start_seite_10, format="%d.%m.%Y - %H:%M:%S")
#results$start_seite_11 <- strptime(results$start_seite_11, format="%d.%m.%Y - %H:%M:%S")

#### Zeitdifferenzen berechnen ####
results$dauer_seite_1 <- results$start_seite_2 - results$start_seite_1
results$dauer_seite_2 <- results$start_seite_3 - results$start_seite_2
results$dauer_seite_3 <- results$start_seite_4 - results$start_seite_3
results$dauer_seite_4 <- results$start_seite_5 - results$start_seite_4
results$dauer_seite_5 <- results$start_seite_6 - results$start_seite_5
results$dauer_seite_6 <- results$start_seite_7 - results$start_seite_6
results$dauer_seite_7 <- results$start_seite_8 - results$start_seite_7
results$dauer_seite_8 <- results$start_seite_9 - results$start_seite_8
results$dauer_seite_9 <- results$start_seite_10 - results$start_seite_9
#results$dauer_seite_10 <- results$start_seite_11 - results$start_seite_10

#### Textergebnisse exportieren ####
write.table(results$p6_5_workshop_themen, str_c(filename, "___p6_5_workshop_themen.txt"), row.names=F, col.names=F, na="")
write.table(results$p8_3_nextcloud_verbesserung, str_c(filename, "___p8_3_nextcloud_verbesserung.txt"), row.names=F, col.names=F, na="")
write.table(results$p8_4_textverarbeitung, str_c(filename, "___p8_4_textverarbeitung.txt"), row.names=F, col.names=F, na="")
write.table(results$p8_5_andere_tools, str_c(filename, "___p8_5_andere_tools"), row.names=F, col.names=F, na="")
write.table(results$p8_6_fehlende_tools, str_c(filename, "___p8_6_fehlende_tools.txt"), row.names=F, col.names=F, na="")
write.table(results$p9_2_gute_eindruecke, str_c(filename, "___p9_2_gute_eindruecke.txt"), row.names=F, col.names=F, na="")
write.table(results$p9_3_schlechte_eindruecke, str_c(filename, "___p9_3_schlechte_eindruecke.txt"), row.names=F, col.names=F, na="")
write.table(results$p9_4_verbesserungen, str_c(filename, "___p9_4_verbesserungen.txt"), row.names=F, col.names=F, na="")

#### Zieldatei schreiben ####
save(results, file=str_c(filename, ".RData"))
